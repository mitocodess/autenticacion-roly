==========================================================================================
EXAMEN DEVOPS - APLICACION WAR DE PRUEBA
==========================================================================================
Tipo de Aplicación : Microservicio
Es una aplicacion de autenticacion (protegida con spring-security)

Necesita conexion a una base de datos postgres, considerar los siguientes datos de conexion:
spring.jpa.database=POSTGRESQL
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=update
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://localhost/demo
spring.datasource.username=postgres
spring.datasource.password=postgres
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
==========================================================================================