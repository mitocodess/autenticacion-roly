package com.exampleroly;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.exampleroly.model.Persona;
import com.exampleroly.model.Usuario;
import com.exampleroly.repo.IPersonaRepo;
import com.exampleroly.repo.IUsuarioRepo;

@SpringBootTest
class DemoWebApplicationTests {
	
	@Autowired
	private IPersonaRepo repoPer;
	
	@Autowired
	private IUsuarioRepo repo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Test
	void crearUsuarioTest() {
		Usuario u=new Usuario();
		u.setId(2);
		u.setNombre("Roly2");
		u.setClave(encoder.encode("123"));
		Usuario uRetorno = repo.save(u);
		
		assertTrue(uRetorno.getClave().equalsIgnoreCase(u.getClave()));
	}
	
	@Test
	void crearPersonaTest() {
		Persona p=new Persona();
		p.setId(5);
		p.setNombre("Nicos");
		
		Persona retorno = repoPer.save(p);
		
		assertTrue(retorno.getNombre().equalsIgnoreCase(p.getNombre()));
	}

}
