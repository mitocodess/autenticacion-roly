package com.exampleroly.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exampleroly.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer>  {

}
