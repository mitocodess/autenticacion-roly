package com.exampleroly.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exampleroly.model.Persona;
import com.exampleroly.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer>  {

	Usuario findByNombre(String nombre);
}
