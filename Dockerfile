FROM openjdk:11
WORKDIR /workspace
COPY target/autenticacion-roly-*.jar app.jar
EXPOSE 9090
ENTRYPOINT [ "java", "-jar", "/workspace/app.jar" ]
